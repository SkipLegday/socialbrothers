package com.example.rikde_000.myapplication.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rikde_000.myapplication.R;
import com.example.rikde_000.myapplication.RVAdapter;
import com.example.rikde_000.myapplication.model.Company;

import java.util.ArrayList;
import java.util.List;

public class Item1Fragment extends Fragment {

    private List<Company> companies;
    private RecyclerView rv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_item1, container, false);

        getActivity().setTitle("List of companies");

        rv=(RecyclerView)view.findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        initializeData();
        initializeAdapter();

        return view;
    }

    private void initializeData(){
        companies = new ArrayList<>();
        companies.add(new Company("Social Brothers", "Maliebaan 42, 3581 CR Utrecht", R.drawable.favicon_white));
        companies.add(new Company("Google Inc.", "Claude Debussylaan 34, 1082 MD Amsterdam", R.drawable.google));
        companies.add(new Company("ING", "Bijlmerdreef 24, 1102 CT Amsterdam-Zuidoost", R.drawable.ing));
        companies.add(new Company("Dummy Title", "Dummy Address", R.drawable.sample));
        companies.add(new Company("Dummy Title", "Dummy Address", R.drawable.sample));
        companies.add(new Company("Dummy Title", "Dummy Address", R.drawable.sample));
        companies.add(new Company("Dummy Title", "Dummy Address", R.drawable.sample));
        companies.add(new Company("Dummy Title", "Dummy Address", R.drawable.sample));
        companies.add(new Company("Dummy Title", "Dummy Address", R.drawable.sample));
        companies.add(new Company("Dummy Title", "Dummy Address", R.drawable.sample));
    }

    private void initializeAdapter(){
        RVAdapter adapter = new RVAdapter(companies);
        rv.setAdapter(adapter);
    }

}
