package com.example.rikde_000.myapplication;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rikde_000.myapplication.model.Company;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.CompanyViewHolder> {

    public static class CompanyViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView companyName;
        TextView companyAddress;
        ImageView companyPhoto;

        CompanyViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            companyName = (TextView)itemView.findViewById(R.id.company_name);
            companyAddress = (TextView)itemView.findViewById(R.id.company_address);
            companyPhoto = (ImageView)itemView.findViewById(R.id.company_photo);
        }
    }

    List<Company> companies;

    public RVAdapter(List<Company> companies){
        this.companies = companies;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CompanyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        CompanyViewHolder cvh = new CompanyViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(CompanyViewHolder companyViewHolder, int i) {
        companyViewHolder.companyName.setText(companies.get(i).getName());
        companyViewHolder.companyAddress.setText(companies.get(i).getAddress());
        companyViewHolder.companyPhoto.setImageResource(companies.get(i).getPhotoId());
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }
}
