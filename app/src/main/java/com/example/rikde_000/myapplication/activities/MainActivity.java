package com.example.rikde_000.myapplication.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.rikde_000.myapplication.fragments.Item1Fragment;
import com.example.rikde_000.myapplication.fragments.Item2Fragment;
import com.example.rikde_000.myapplication.R;
import com.example.rikde_000.myapplication.fragments.Item3Fragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabSelectedListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragment = new Item1Fragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.contentFragment, fragment);
        transaction.commit();

        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.show();

        BottomBar bottomBar = BottomBar.attach(this, savedInstanceState);
        bottomBar.setItemsFromMenu(R.menu.bottom, new OnMenuTabSelectedListener() {
            @Override
            public void onMenuItemSelected(int itemId) {
                Fragment fragment;
                switch (itemId) {
                    case R.id.item_1:
                        fragment = new Item1Fragment();
                        break;
                    case R.id.item_2:
                        fragment = new Item2Fragment();
                        break;
                    case R.id.item_3:
                        fragment = new Item3Fragment();
                        break;
                    default:
                        fragment = new Item1Fragment();
                        break;
                }

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                transaction.replace(R.id.contentFragment, fragment);
                transaction.commit();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity (new Intent(this, SettingsActivity.class));
                break;
        }
        return true;
    }
}
