package com.example.rikde_000.myapplication.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.example.rikde_000.myapplication.JsonReader;
import com.example.rikde_000.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Item2Fragment extends Fragment {

    private WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item2, container, false);
        getActivity().setTitle("Content from API");

        final AsyncTask<String, Void, JSONArray> execute = new JsonReader().execute("https://baconipsum.com/api/?type=meat-and-filler");

        String apiText = null;
        try {
            apiText = execute.get().toString();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        webView = (WebView) view.findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        String customHtml = "<html><body><p>"+apiText+"</p> </body></html>";
        webView.loadData(customHtml, "text/html", "UTF-8");

        return view;
    }
}
