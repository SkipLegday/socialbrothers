package com.example.rikde_000.myapplication.model;

/**
 * Created by rikde_000 on 28-6-2016.
 */
public class Company {
    private String name;
    private String address;
    private int photoId;

    public Company(String name, String address, int photoId) {
        this.setName(name);
        this.setAddress(address);
        this.setPhotoId(photoId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
