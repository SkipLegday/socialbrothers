package com.example.rikde_000.myapplication;

import android.os.AsyncTask;
import android.renderscript.ScriptGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rikde_000 on 28-6-2016.
 */
public class JsonReader extends AsyncTask<String, Void, JSONArray> {
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    @Override
    protected JSONArray doInBackground(String... urls) {
        InputStream is = null;
        JSONArray json = null;
        try {
            is = new URL(urls[0]).openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            System.out.println("TEXT!!!!!!!!!!: "+jsonText);
            json = new JSONArray(jsonText);
            System.out.println("JSON TEXT!!!!!!!!!!: "+json.toString());
            //return json;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return json;
    }
}
